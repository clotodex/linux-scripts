#!/bin/bash

COLOR_NC='\e[0m' # No Color
COLOR_WHITE='\e[1;37m'
COLOR_LIGHT_GREEN='\e[1;32m'
COLOR_LIGHT_RED='\e[1;31m'
COLOR_YELLOW='\e[1;33m'

###########
# LOGGING #
###########

# create fd to handle logging
exec 3>&1

info () {
	echo -e "$COLOR_WHITE[INFO]$COLOR_NC $@" >&3
}
ok () {
	echo -e "$COLOR_LIGHT_GREEN[OK]$COLOR_NC $@" >&3
}
warning () {
	echo -e "$COLOR_YELLOW[WARNING]$COLOR_NC $@" >&3
}
urgent () {
	echo -e "$COLOR_LIGHT_RED[URGENT]$COLOR_NC $@" >&3
}


##########
# SCRIPT #
##########

# check if kernel is compiled
# uname vs symlink
# TODO check against kernel.org
version="$(uname -r)"
symlink="$(readlink /usr/src/linux)"
latest="$(curl -s https://www.kernel.org/ | grep -A1 'stable:' | grep -oP '(?<=strong>).*(?=</strong.*)' | head -1)"

if test "${symlink#*$latest}" != "$symlink"
then
	ok "Latest kernel version installed!"
else
	warning "Newer kernel version available! $latest"
fi

if [ "linux-$version" == "$symlink" ]; then
	ok "Installed kernel is compiled: ($version)"
else
	urgent "Please compile your kernel!"
	urgent "current version: $version, available version: $symlink"
fi

# check for updates (only sync when longer than x hours ago)
#TODO check when last eix-sync was
max_updates=50
world="$(eix -u -c --world | grep -P '\[[^\]]*U' | wc -l)"
total="$(eix -u -c | grep -P '\[[^\]]*U' | wc -l)"

if [[ "$world" -eq 0 ]]
then
	ok "system up to date ($total library updates available"
else
	if [ "$total" -ge "$max_updates" ]
	then
		urgent "You should really consider updating your system! world: $world, total: $total!"
	else
		warning "Updates available! world: $world, total: $total!"
	fi
fi

# check battery life status
good=95
ok=90
used=80
#bad = lower
capacity="$(acpi -i | grep capacity | egrep -o "[0-9]*%" | tr -d '%')"

if [ "$capacity" -gt "$good" ]; then
	ok "Your battery seems fine, capacity: $capacity%"
elif [ "$capacity" -gt "$ok" ]; then
	ok "Your battery is still ok, capacity: $capacity%"
elif [ "$capacity" -gt "$used" ]; then
	ok "Your battery is getting worse, capacity: $capacity%"
else
	ok "It seems your battery is slowly dying, capacity: $capacity%"
fi

# TODO check hard drive health?

# TODO check git repos - uncommited, fetch everything, etc - and print status (use ssh agent!!)

# print hard drive free space
warning=80
bad=90

used_percent="$(df / -mh --output="pcent" | tail -1 | egrep -o "[0-9]*")"

if [ "$used_percent" -gt "$bad" ]; then
	urgent "Your hard drive is almost full! $used_percent%"
elif [ "$used_percent" -gt "$warning" ]; then
	warning "Your hard drive is filling up! $used_percent%"
else
	ok "hard drive usage is under $warning%"
fi
info "free space: $(df / -mh --output="avail" | tail -1 | tr -d ' ')"

# hard drive health
if [ "$EUID" -ne 0 ]
then
	warning "Hard drive health check can only be performed if you have root access!"
else
	smartctl -H /dev/sda | grep -q PASSED && ok "Last SMART test was successfully passed." || urgent "Last SMART test FAILED!"
fi

# sync time
if [ "$EUID" -ne 0 ]
then
	warning "System time check can only be performed if you have root access!"
else
	#max one second
	max_offset=1
	offset="$(ntpdate -q ptbtime1.ptb.de | egrep -o "offset.*sec" | egrep -o "[-.0-9]*")"
	offset="${offset#-}"

	if (( $(bc <<< "$offset >= $max_offset") ))
	then
		warning "ntp is not in sync anymore - check if deamon is running or use ntpdate"
		warning "offset: $offset seconds"
		info "syncing time now"
		ntpdate ptbtime1.ptb.de
	else
		ok "time is synced, offset: $offset seconds"
	fi
fi

# TODO print project todos :D

# uptime? :D
info "uptime: $(uptime)"
