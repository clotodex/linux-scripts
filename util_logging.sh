COLOR_NC='\e[0m' # No Color
COLOR_WHITE='\e[1;37m'
COLOR_LIGHT_GREEN='\e[1;32m'
COLOR_LIGHT_RED='\e[1;31m'
COLOR_YELLOW='\e[1;33m'

###########
# LOGGING #
###########

# create fd to handle logging
exec 3>&1

info () {
	echo -e "$COLOR_WHITE[INFO]$COLOR_NC $@" >&3
}
ok () {
	echo -e "$COLOR_LIGHT_GREEN[OK]$COLOR_NC $@" >&3
}
warning () {
	echo -e "$COLOR_YELLOW[WARNING]$COLOR_NC $@" >&3
}
error () {
	echo -e "$COLOR_LIGHT_RED[ERROR]$COLOR_NC $@" >&3
}
die () {
	error "$@"
	exit 1
}
